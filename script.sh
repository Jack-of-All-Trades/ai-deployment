#!/bin/bash

Hostname=$1
IP=$2
Hostfile=./provisioning/hosts
Privatekey=$(pwd)/newkey

[ -f $Hostfile ] && rm $Hostfile

cd provisioning; echo -e "[prod]\n$Hostname ansible_host=$IP ansible_ssh_private_key_file=$Privatekey" >> hosts
# ansible all -m ping -vvvvv
# ansible prod -m ping -vvvvv
# ansible vault password
echo "mlchatbot" > .vault_pass.txt; chmod 0600 .vault_pass.txt
ansible-playbook site.yml