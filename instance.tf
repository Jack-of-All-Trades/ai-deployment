resource "digitalocean_droplet" "ml-prod" {
  image    = "${var.IMAGE}"
  name     = "ml-prodserver"
  region   = "${var.REGION}"
  size     = "${var.SIZE[1]}"
  ssh_keys = ["${digitalocean_ssh_key.primarykey.fingerprint}"]
  tags     = ["${digitalocean_tag.default.id}"]
  provisioner "local-exec" {
    command = "chmod +x ./script.sh; ./script.sh ${digitalocean_droplet.ml-prod.name} ${digitalocean_droplet.ml-prod.ipv4_address}"
  }
  connection {
    user        = "${var.INSTANCE_USERNAME}"
    type        = "ssh"
    private_key = "${file("${var.PRIVATE_KEY}")}"
    agent       = "true"
    host        = "${digitalocean_droplet.ml-prod.ipv4_address}"
  }
}
