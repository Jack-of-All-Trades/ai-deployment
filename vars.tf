variable "TOKEN" {}

variable "REGION" {
  default = "sgp1"
}

variable "INSTANCE_USERNAME" {
  default = "root"
}
variable "PUBLIC_KEY" {
  default = "newkey.pub"
}

variable "PRIVATE_KEY" {
  default = "newkey"
}

variable "IMAGE" {
  default = "ubuntu-16-04-x64"
}

variable "SIZE" {
  type = "list"
  default = [
    "s-4vcpu-8gb", //monthly price 40
    "s-2vcpu-4gb", //monthly price 20
    "s-1vcpu-3gb", // monthly price 15 with 1 cpu
    "s-2vcpu-2gb", // monthly price 15 with 2 cpus
    "s-1vcpu-2gb", // monthly price 10
    "s-1vcpu-1gb"  // monthly price 5
  ]
}
