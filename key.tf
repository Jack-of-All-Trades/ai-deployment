resource "digitalocean_ssh_key" "primarykey" {
  name       = "newkey"
  public_key = "${file("${var.PUBLIC_KEY}")}"
}
